module.exports = {
  endpoint: 'http://gitea:3000/api/v1/',
  token: '3278bb9f093334b22a1cc63ce6b6cd6c55b3e92c',
  gitAuthor: 'renovate-bot <foo.bar@baz.de>',
  platform: 'gitea',
  onboardingConfig: {
    extends: ['config:base']
  },
  autodiscover: true,
}
